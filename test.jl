using ColorSchemes
using Images

function julia_iter(z::ComplexF64, c::ComplexF64, max_iter::Int64, ϵ::Float64)::Int
    
    """ Given a point z in the complex plane, calculate the function value
    
    :param z: point in the complex plane
    :param c: complex seed of the fractal
    :param max_iter: maximum number of iterations 
    :param ϵ: escape criterion, set to R > 0 such that R**2 - R >= sqrt(Re(c)^2 + Im(c)^2)
    
    :return: function value, corresponding to the color in the fractal as a range between 0 and 255 (maximum)
    
    """
    
    for i in 1:max_iter

        z = z^2 + c
        
        if abs2(z) > ϵ
            
            return i
            
        end 
    
    end

    return 255 # corresponding to maximum possible color value 

end

function compute_fractal2(center::Tuple{Float64,Float64}, dimensions::Tuple{Int64,Int64}, scale::Float64, image::Matrix{UInt8}, max_iter::Int64, c::ComplexF64)
    
    """ Given a bounding box of dimensions MxN and a 
    
    
    """
   
    height, width = dimensions
    
    # scaling only occurs in the x-dimension!
    min_x = center[1] - scale
    max_x = center[1] + scale
    
    min_y = center[2]-(scale/(width/height))
    max_y = center[2]+(scale/(width/height))

    pixel_size_x = (max_x - min_x) / height
    #pixel_size_y = (max_y - min_y) / width
    
	#Threads.@threads for x in 1:width
    for x in 1:width

        real = min_x + (x - 1) * pixel_size_x
        
        for y in 1:height
        
            imag = min_y + (y - 1) * pixel_size_x
            color = julia_iter(complex(real, imag), c, max_iter, 4.0)
			#=image[y, x] = reinterpret(N0f8, UInt8(color))=#
			image[y, x] = color
			#=image[y, x] = get(ColorSchemes.viridis, color/255)=#
        
        end
    
    end

end

using PyPlot
#=using Images=#
#=using FileIO=#
#=using ImageIO=#
#
using VideoIO

using Base.Threads


function rotate_video(center::Tuple{Float64,Float64}, dimensions::Tuple{Int64,Int64}, scale::Float64, max_iter::Int64, fps::Int64)

	images = Array{Matrix{RGB{N0f8}}}(undef, 126)

	# looping from 0 to 4π and dividing by 2 (to get 2π, a full rotation) later to make the video slower
	Threads.@threads for i in 0:125
		
		image = zeros(UInt8, dimensions)
		
		c = exp((i/200)*im)
		
		compute_fractal2(center, dimensions, scale, image, max_iter, c)
		
		# save to stack and add colorscheme
		# unfortunately, get(ColorSchemes.xxx) returns a Matrix{RGB{Float64}} even though
		images[i+1] = get(ColorSchemes.viridis, image/255)
		
	end


	encoder_options = (crf=21, preset="slow")
	VideoIO.save("video.mp4", images, framerate=fps, encoder_options=encoder_options, thread_count=8, target_pix_fmt=VideoIO.AV_PIX_FMT_YUV420P)

end

x, y = (0.0, 0.0)
max_iter = 200
scale = 1.3
dimensions = (1080, 1920)
center = (-0.8, -0.5)
fps = 60

#rotate_video(center, dimensions, scale, max_iter, fps)

function zoom_video(center::Tuple{Float64,Float64}, dimensions::Tuple{Int64,Int64}, scale::Float64, max_iter::Int64, c::ComplexF64, fps::Int64)

	println("Running on ", nthreads(), " threads")

    print("Starting zoom video... ")
    
    # allocating stack of images with type N0f8, which is a scaled (Normed) [0:1] version of the usual [0:255] RGB values
    images = Array{Matrix{RGB{N0f8}}}(undef, 1801)

	# 1800 frames at 60 fps gives 30 seconds of video
    Threads.@threads for i in 0:1800

		zoom_scale = scale*(0.993^float(i))

        image = zeros(UInt8, dimensions)
        compute_fractal2(center, dimensions, zoom_scale, image, max_iter, c)

        # save to stack and add colorscheme
        images[i+1] = get(ColorSchemes.viridis, image/255)
        
		# scale = start_scale 
        #=scale *= 0.998=#

    end
    
    print("Done\n")
    
    print("Saving video... ")

    encoder_options = (crf=21, preset="slow")
    VideoIO.save("zoom_video.mp4", images, framerate=fps, encoder_options=encoder_options, thread_count=8, target_pix_fmt=VideoIO.AV_PIX_FMT_YUV420P)

    print("Done\n")
    
end


center = (-0.393772, 0.00717)
c = -0.06 + 0.67im # seed
dimensions = (1080, 1920) # height, width
max_iter = 120

# initial scale
scale = 1.3

fps = 60

#=@time zoom_video(center, dimensions, scale, max_iter, c, fps)=#
#
function make_video(center::Tuple{Float64,Float64}, 
                    dimensions::Tuple{Int64,Int64}, 
                    scale::Float64, 
                    c::ComplexF64, 
                    max_iter::Int64, 
                    fps::Int64, 
                    video_type::String, 
                    n_frames::Int64,
                    colorscheme::ColorScheme, 
                    fname::String)

    # allocating stack of images with type N0f8, which is a scaled (Normed) [0:1] version of the usual [0:255] RGB values
    image_stack = Array{Array{RGB{N0f8},2}}(undef, n_frames)

    Threads.@threads for i in 1:n_frames
        
        if video_type == "max_iter"
            max_iter = i
        elseif video_type == "zoom"
            scale = scale*(0.993^i)
        elseif video_type == "seed"
            c = exp(((i-1)/200)*im)
        else
            ErrorException("Unknown input to video_type")
        end
            
        image = zeros(UInt8, dimensions)
        compute_fractal2(center, dimensions, scale, image, max_iter, c)

        # save to stack and add colorscheme
        image_stack[i] = get(colorscheme, image/255)

    end

    encoder_options = (crf=21, preset="slow")
    
    # for some reason, VideoIO defaulted to pix_fmt=yuv422p
    VideoIO.save(fname, image_stack, framerate=fps, encoder_options=encoder_options, thread_count=nthreads(), target_pix_fmt=VideoIO.AV_PIX_FMT_YUV420P)

end

c = -0.06 + 0.67im # seed
scale = 1.3
dimensions = (1080, 1920) # height, width
center = (-1.0, -0.5)
max_iter = 200
fps = 20
video_type = "max_iter"
n_frames = 255
colorscheme = ColorSchemes.viridis
fname = "iter_video.mp4"


make_video(center, dimensions, scale, c, max_iter, fps, video_type, n_frames, colorscheme, fname)




