# JuliaFract - Julia Fractals in Julia

Implementation of the [Julia fractal](https://en.wikipedia.org/wiki/Julia_set) in the [Julia programming language](https://julialang.org/).
The Julia language was chosen mostly to check it out; the goal of the language is, [among other things](https://docs.julialang.org/en/v1/), to be as fast as C or Fortran, as expressive as Python, as good for statistics as R, to allow massively parallelization on the CPU and offloading work to GPUs as well as easy interfacing to beforementioned programming languages. The language is 100% open source and was launched in 2012 has quite a lot of momentum. I imagine it will be quite beneficial to the HPC community.  

This project is also a playground for experimenting with parallelization, optimizations, data visualization and running stuff on the GPU (when I get a new computer). 

The code is in a Jupyter notebook (technically an IJulia notebook) but could trivially be exported to a pure Julia program. 

## How to run

### Locally

1. Clone the repo 
2. Make sure you have Julia installed. Only tested with version 1.6.3 but should work with 1.5 as well. 
3. Run `using Pkg; activate` from the Julie REPL (invoked with `julia` from the shell). This installs all dependencies (sort of like `pip install -r requirements.txt` for Python projects)
4. Run the notebook either with `jupyter notebook` from the shell or `using IJulia; notebook()` from the Julia REPL
5. Read the setup section of the notebook

### With Binder

You can also run the notebook with Binder: [https://mybinder.org/v2/gl/philip-jakobsen%2Fjuliafract/HEAD](https://mybinder.org/v2/gl/philip-jakobsen%2Fjuliafract/HEAD).
Binder is a free service to run Jupyter/IJulia notebooks on the cloud that also takes care of all the project dependencies and so on.  
It might take some time to launch and also the limit is 2 GB memory and only 1 CPU core, so the multithreading parts are of no benefit. 
The future GPU code won't run for obvious reasons.
